import * as vscode from "vscode";
import * as gitlab from "./gitlab";
import { Resources } from "./resources";
import { VscodeGitlabService } from "./vscode-gitlab-service";

type Query = {
  label: string;
  id: string;
  get: (
    apiBuilder: gitlab.ApiBuilder
  ) => Promise<Query[] | gitlab.ApiResult<gitlab.MergeRequestSummary[]>>;
};

type MRApi = {
  mergeRequests: (
    query?: gitlab.MRFilter
  ) => Promise<gitlab.ApiResult<gitlab.MergeRequestSummary[]>>;
};

const getQueriesByState = (api: MRApi, idPrefix: string): Promise<Query[]> => {
  return Promise.resolve([
    {
      label: "Opened",
      id: idPrefix + "-opened",
      get: () => api.mergeRequests({ state: "opened" }),
    },
    {
      label: "Merged",
      id: idPrefix + "-merged",
      get: () => api.mergeRequests({ state: "merged" }),
    },
    {
      label: "Closed",
      id: idPrefix + "-closed",
      get: () => api.mergeRequests({ state: "closed" }),
    },
  ]);
};

const toError = (details: gitlab.ApiError): ErrorItem => {
  return {
    id: "error",
    message: `Unable to get data from the server: ${details}`,
  };
};

const Queries: Query[] = [
  {
    label: "My Merge Requests",
    id: "my-mrs",
    get: (apiBuilder: gitlab.ApiBuilder) =>
      getQueriesByState(apiBuilder.api, "project"),
  },
  {
    label: "By Group",
    id: "by-org",
    get: async (apiBuilder: gitlab.ApiBuilder) => {
      let result = await apiBuilder.api.groups();
      if (result.success) {
        return result.result.map((group: gitlab.Group) => {
          return {
            label: group.name,
            id: "group-" + group.id.toString(),
            get: (apiBuilder: gitlab.ApiBuilder) =>
              getQueriesByState(apiBuilder.group(group.id).api, "group"),
          };
        });
      } else {
        return result;
      }
    },
  },
  {
    label: "By Project",
    id: "by-proj",
    get: async (apiBuilder: gitlab.ApiBuilder) => {
      let result = await apiBuilder.api.projects({
        membership: true,
        with_merge_requests_enabled: true,
      });
      if (result.success) {
        return result.result.map((proj: gitlab.Project) => {
          return {
            label: proj.name,
            id: "project-" + proj.id.toString(),
            get: (apiBuilder: gitlab.ApiBuilder) =>
              getQueriesByState(apiBuilder.project(proj.id).api, "project"),
          };
        });
      } else {
        return result;
      }
    },
  },
];

type ErrorItem = {
  id: "error";
  message: string;
};

type TreeDataItem = ParentedItem<
  gitlab.MergeRequestSummary | Query | ErrorItem
>;

type ParentedItem<T> = {
  parent?: any;
  data: T;
};

const treeId = (item: TreeDataItem): string => {
  return item.parent ? `${treeId(item.parent)}/${item.data.id}` : "";
};

const isQuery = (
  element?: gitlab.MergeRequestSummary | Query
): element is Query => !!element && !!(element as any).get;

const isErrorItem = (
  element?: gitlab.MergeRequestSummary | Query | ErrorItem
): element is ErrorItem => {
  return !!(element as any).message;
};

export class QueryViewProvider
  implements vscode.TreeDataProvider<TreeDataItem> {
  private api: gitlab.Api;
  constructor(
    private gitlab: VscodeGitlabService,
    private resources: Resources
  ) {
    this.api = gitlab.api();
    gitlab.on("userChanged", () => this.refresh());
  }

  private _onDidChangeTreeData: vscode.EventEmitter<TreeDataItem> = new vscode.EventEmitter<TreeDataItem>();

  readonly onDidChangeTreeData: vscode.Event<TreeDataItem> = this
    ._onDidChangeTreeData.event;

  refresh() {
    this._onDidChangeTreeData.fire();
  }

  getTreeItem(
    element: TreeDataItem
  ): vscode.TreeItem | Thenable<vscode.TreeItem> {
    if (isErrorItem(element.data)) {
      return {
        contextValue: "error",
        label: element.data.message,
        id: treeId(element),
        iconPath: this.resources.error,
      };
    } else if (isQuery(element.data)) {
      return {
        contextValue: "query",
        label: element.data.label,
        id: treeId(element),
        collapsibleState: vscode.TreeItemCollapsibleState.Collapsed,
      };
    } else {
      return {
        contextValue: "merge-request",
        label: element.data.title,
        id: treeId(element),
        collapsibleState: vscode.TreeItemCollapsibleState.None,
        command: {
          title: "",
          command: "gitlabReview.setCurrentMergeRequest",
          arguments: [element.data],
        },
      };
    }
  }

  getChildren(element?: TreeDataItem): vscode.ProviderResult<TreeDataItem[]> {
    if (!element) {
      return Queries.map((q) => {
        return { parent: null, data: q };
      });
    }
    if (isErrorItem(element.data)) {
      return null;
    }
    if (isQuery(element.data)) {
      return element.data
        .get(new gitlab.ApiBuilder(this.api))
        .then((result) => {
          let results;
          if (Array.isArray(result)) {
            return result.map((mr) => {
              return { parent: element, data: mr };
            });
          } else if (result.success) {
            return result.result.map((mr) => {
              return { parent: element, data: mr };
            });
          } else {
            return [{ parent: element, data: toError(result.details) }];
          }
        });
    }
  }

  getParent?(element: TreeDataItem): vscode.ProviderResult<TreeDataItem> {
    return element.parent;
  }
}
