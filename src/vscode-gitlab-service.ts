import * as gitlab from "./gitlab";
import * as vscode from "vscode";
import { EventEmitter } from "events";
import { unwrap } from "./gitlab";

const CFG_SECTION = "gitlabReview";

export const credentials = (): gitlab.Credentials => {
  const config = vscode.workspace.getConfiguration(CFG_SECTION);
  const instance = config.get<string>("instance") || "https://gitlab.com";
  const token = config.get<string>("token") || "";
  return { instance, token };
};

export const updateCredentials = (creds: gitlab.Credentials) => {
  const config = vscode.workspace.getConfiguration(CFG_SECTION);
  config.update("instance", creds.instance);
  config.update("token", creds.token);
};

export class VscodeGitlabService extends EventEmitter {
  private _api: gitlab.Api;
  private _user: gitlab.FullUser | undefined;
  private _credentials: gitlab.Credentials;

  api(): gitlab.Api {
    return this._api;
  }

  user(): gitlab.FullUser | undefined {
    return this._user;
  }

  private async getUser(): Promise<gitlab.FullUser | undefined> {
    try {
      return unwrap(await this._api.self(), undefined);
    } catch (e) {
      return undefined;
    }
  }

  async init() {
    this._user = await this.getUser();
  }

  constructor(context: vscode.ExtensionContext) {
    super();
    this._credentials = credentials();
    this._api = new gitlab.Api(this._credentials);

    context.subscriptions.push(
      vscode.workspace.onDidChangeConfiguration(async (e) => {
        if (e.affectsConfiguration(CFG_SECTION)) {
          const newCredentials = credentials();
          this._api.setCredentials(newCredentials);
          const result = await this._api.self();
          const newUser = result.success ? result.result : undefined;
          if (this._user !== newUser) {
            if (
              this._user?.id !== newUser?.id ||
              this._credentials.instance !== newCredentials.instance
            ) {
              this._user = newUser;
              this.emit("userChanged", newUser);
            }
          }
          this.emit("apiCredentialsChanged");
        }
      })
    );
  }
}
