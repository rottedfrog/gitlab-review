import { Folder } from "./folder";
import * as vscode from "vscode";
import * as gitlab from "../gitlab";
import { Resources } from "../resources";
import { attachDiffTree } from "./diff-tree";
import { gitlabEncode } from "../gitlab-content-provider";
import { MRCommentManager } from "../comments";
import { VscodeGitlabService } from "../vscode-gitlab-service";

type MR = gitlab.MergeRequest & gitlab.Changes;
type ParentedItem<T> = {
  parent?: any;
  data: T;
};
type OutlineItem = MR | Folder | ParentedItem<gitlab.Change>;

const isMergeRequest = (item: any): item is MR => item.iid && item.merge_status;

const getOpenDiffCommand = (
  mr: gitlab.MergeRequest,
  changes: gitlab.Change
): vscode.Command | undefined => {
  if (!mr.diff_refs.base_sha) {
    return undefined;
  }
  const baseFile = gitlabEncode(
    mr.project_id,
    changes.old_path || changes.new_path,
    mr.diff_refs.base_sha
  );
  const headFile = gitlabEncode(
    mr.project_id,
    changes.new_path,
    mr.diff_refs.head_sha
  );
  const parts = changes.new_path.split("/");
  const filename = parts[parts.length - 1];
  return {
    title: "",
    command: "vscode.diff",
    arguments: [baseFile, headFile, filename]
  };
};

export class MergeRequestOutlineViewProvider
  implements vscode.TreeDataProvider<OutlineItem> {
  private _mr: (gitlab.MergeRequest & gitlab.Changes) | undefined;
  private mrApi: gitlab.MergeRequestApi | undefined;
  private commentManager: MRCommentManager | null;
  private _folderStructure: OutlineItem[] = [];

  public async init(value: gitlab.MergeRequestSummary | undefined) {
    this.commentManager?.dispose();
    this.commentManager = null;
    const user = this.gitlab.user();
    if (value && user) {
      const apiBuilder = new gitlab.ApiBuilder(this.gitlab.api())
        .project(value.project_id)
        .mergeRequest(value.iid);
      this.mrApi = apiBuilder.api;
      let result = await this.mrApi.changes();
      if (result.success) {
        this._mr = result.result;
      } else {
        this._mr = undefined;
      }
      if (this._mr) {
        let discussions = await this.mrApi.discussions();

        if (discussions.success) {
          this.commentManager = new MRCommentManager(
            this._mr,
            apiBuilder,
            discussions.result,
            user
          );
        }
      }
      this.changes = new Folder("Changes");
      this._onDidChangeTreeData.fire(null);
    }
  }

  get comments(): MRCommentManager | null {
    return this.commentManager;
  }

  constructor(
    private gitlab: VscodeGitlabService,
    private resources: Resources
  ) {
    // if the user changes, invalidate the review currently underway.
    gitlab.on("userChanged", () => this.init(undefined));
    this.commentManager = null;
  }

  public get api(): gitlab.MergeRequestApi | undefined {
    return this.mrApi;
  }

  public get mergeRequest(): gitlab.MergeRequest | undefined {
    return this._mr;
  }

  public name(): string | undefined {
    if (this._mr) {
      return `!${this._mr.iid} - ${this._mr.title}`;
    }
  }

  private getDetailsItem(): vscode.TreeItem {
    return {
      id: "details",
      label: "Details",
      command: {
        title: "",
        command: "gitlabReview.showMergeRequestDetails",
        arguments: [this._mr]
      }
    };
  }

  private changes = new Folder("Changes");

  private getIconForFile(change: gitlab.Change): vscode.Uri {
    if (change.renamed_file) {
      return this.resources.statusRenamed;
    }
    if (change.deleted_file) {
      return this.resources.statusDeleted;
    }
    if (change.new_file) {
      return this.resources.statusAdded;
    }
    return this.resources.statusModified;
  }

  private getTooltipForFile(change: gitlab.Change): string | undefined {
    let status = "";
    let text = "";
    if (change.renamed_file) {
      text = `Renamed from ${change.old_path} to ${change.new_path}`;
      if (change.a_mode && change.b_mode && change.a_mode !== change.b_mode) {
        status =
          "changed permissions from ${change.a_mode} to ${change.b_mode}";
      }
    }
    if (change.deleted_file) {
      text = "Deleted";
    }
    if (change.new_file) {
      text = "New";
      status = `set permissions to ${change.b_mode}`;
    }
    return [text, status].filter(x => x).join(", ") || undefined;
  }

  getTreeItem(
    element: OutlineItem
  ): vscode.TreeItem | Thenable<vscode.TreeItem> {
    if (element instanceof vscode.TreeItem) {
      return element;
    } else if (isMergeRequest(element)) {
      return this.getDetailsItem();
    } else {
      const parts = element.data.new_path.split("/");
      const filename = parts[parts.length - 1];
      const item = new vscode.TreeItem(
        parts[parts.length - 1],
        vscode.TreeItemCollapsibleState.None
      );
      if (this._mr && this._mr.diff_refs) {
        item.command = getOpenDiffCommand(this._mr, element.data);
      }
      item.iconPath = this.getIconForFile(element.data);
      item.tooltip = this.getTooltipForFile(element.data);
      return item;
    }
  }

  getChildren(element?: OutlineItem): vscode.ProviderResult<OutlineItem[]> {
    if (!element && this._mr) {
      attachDiffTree(this.changes, this._mr.changes);
      //Uncomment this to add the web view back in
      return [/*this._mr,*/ this.changes] as OutlineItem[];
    } else if (element instanceof Folder) {
      return element.children;
    }
    return null;
  }

  getParent?(element: OutlineItem): vscode.ProviderResult<OutlineItem> {
    return isMergeRequest(element) ? null : element.parent;
  }

  private _onDidChangeTreeData: vscode.EventEmitter<
    OutlineItem | null | undefined
  > = new vscode.EventEmitter<OutlineItem | null | undefined>();
  readonly onDidChangeTreeData: vscode.Event<
    OutlineItem | null | undefined
  > = this._onDidChangeTreeData.event;
}
