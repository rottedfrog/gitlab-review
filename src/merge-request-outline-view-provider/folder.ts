import * as vscode from "vscode";
import * as gitlab from "../gitlab";

export type ParentedItem<T> = {
  parent?: any;
  data: T;
};

// Represents a folder in the repo file structure.
// Used to visualize a tree of changed files in the
// MergeRequestOutlineProvider
export class Folder extends vscode.TreeItem {
  constructor(label: string, private _parent?: Folder) {
    super(label, vscode.TreeItemCollapsibleState.Expanded);
    this.iconPath = vscode.ThemeIcon.Folder;
    this.children = [];
  }

  children: (Folder | ParentedItem<gitlab.Change>)[];

  parent(): Folder | undefined {
    return this._parent;
  }
}
