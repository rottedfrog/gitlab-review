import * as gitlab from "../gitlab";
import { Folder } from "./folder";

class Path {
  private pathParts: string[];
  constructor(path: string) {
    this.pathParts = path.split("/");
  }

  public get filename(): string {
    return this.pathParts[this.pathParts.length - 1];
  }

  public get folders(): string[] {
    return this.pathParts.slice(0, this.pathParts.length - 1);
  }
}

const getFolder = (parent: Folder, name: string): Folder => {
  let index = parent.children.findIndex(
    folder => folder instanceof Folder && folder.label === name
  );
  if (index >= 0) {
    return parent.children[index] as Folder;
  }
  let folder = new Folder(name, parent);
  parent.children.push(folder);
  return folder;
};

// Take the changes and create a tree of folders and merge items,
// and attach it to the specified root folder.
export const attachDiffTree = (
  rootFolder: Folder,
  changes: gitlab.Change[]
) => {
  for (const change of changes) {
    const path = new Path(change.new_path);

    let folder = rootFolder;
    for (const subfolderName of path.folders) {
      folder = getFolder(folder, subfolderName);
    }
    folder.children.push({ parent: folder, data: change });
  }
};
