export type Pipeline = {
  id: number;
  sha: string;
  ref: string;
  status: string;
  web_url: string;
};
