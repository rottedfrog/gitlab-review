import { User } from "./user";
import { TimeStats } from "./time-stats";
import { Milestone } from "./milestone";
import { Pipeline } from "./pipeline";
import { Change } from "./change";

export type DiffRefs =
  | {
      base_sha: string;
      head_sha: string;
      start_sha: string;
    }
  | {
      base_sha: null;
      head_sha: null;
      start_sha: null;
    };

// Extra fields included when you request a single merge request
export type MergeRequestDetails = {
  user: {
    can_merge: boolean;
  };
  merge_error: string | null;
  changes_count: number;
  latest_build_started_at: string | null;
  latest_build_finished_at: string | null;
  first_deployed_to_production_at: string | null;
  diverged_commits_count: number;
  rebase_in_progress: boolean;
  subscribed: boolean;
  pipeline: Pipeline | null;
  diff_refs: DiffRefs;
};

export type Changes = {
  changes: Change[];
};

export type MergeRequest = MergeRequestSummary & MergeRequestDetails;
export type MergeRequestState = "opened" | "closed" | "locked" | "merged";

// Summary is what you get when you request a list of MR's from an endpoint.
export type MergeRequestSummary = {
  id: number;
  iid: number;
  project_id: number;
  title: string;
  description: string;
  state: MergeRequestState;
  merged_by: User | null;
  merged_at: string | null;
  closed_by: User | null;
  closed_at: string | null;
  created_at: string;
  updated_at: string | null;
  target_branch: string;
  source_branch: string;
  reference: string;
  upvotes: number;
  downvotes: number;
  author: User;
  assignee: User | null;
  assignees: User[];
  source_project_id: number;
  target_project_id: number;
  labels: string[];
  work_in_progress: boolean;
  milestone: Milestone;
  merge_when_pipeline_succeeds: boolean;
  merge_status: string;
  sha: string;
  merge_commit_sha: string | null;
  squash_commit_sha: string | null;
  user_notes_count: number;
  discussion_locked: string | null;
  should_remove_source_branch: boolean | null;
  force_remove_source_branch: boolean;
  allow_collaboration: boolean;
  allow_maintainer_to_push: boolean;
  web_url: string;
  time_stats: TimeStats;
  squash: boolean;
  has_conflicts: boolean;
  blocking_discussions_resolved: boolean;
  task_completion_status: {
    count: number;
    completed_count: number;
  };
};
