export type TimeStats = {
  time_estimate: number;
  total_time_spent: number;
  human_time_estimate: number | null;
  human_total_time_spent: number | null;
};
