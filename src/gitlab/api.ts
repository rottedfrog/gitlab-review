import fetch, { Response } from "node-fetch";
import {
  MergeRequestState,
  MergeRequest,
  MergeRequestSummary,
  Changes,
} from "./merge-request";
import { Discussion, DiffNote } from "./discussion";
import { Result, failure, success } from "./result";
import { Group } from "./group";
import { Project } from "./project";
import { FullUser } from "./user";
import * as EventEmitter from "events";

export enum FailureType {
  ExecutionBlocked,
  BadRequest,
  ServerError,
  Unauthorized,
  NotFound,
  Forbidden,
  CouldNotConnect,
  ServiceUnavailable,
  ImATeapot,
  ProxyAuthenticationRequired,
  RequestTimeout,
  NotImplemented,
  UnknownError,
}

export class ApiError {
  constructor(private _kind: FailureType) {}
  toString(): string {
    switch (this._kind) {
      case FailureType.CouldNotConnect:
        return "Unable to connect";
      case FailureType.ExecutionBlocked:
        return "Disabled (execution blocked)";
      case FailureType.Forbidden:
        return "Permission denied";
      case FailureType.ImATeapot:
        return "I'm a little teapot, short and stout...";
      case FailureType.NotFound:
        return "Not found";
      case FailureType.NotImplemented:
        return "Api not implemented";
      case FailureType.ProxyAuthenticationRequired:
        return "Proxy authentication required";
      case FailureType.RequestTimeout:
        return "Request timed out";
      case FailureType.ServerError:
        return "Server error";
      case FailureType.ServiceUnavailable:
        return "Service unavailable";
      case FailureType.Unauthorized:
        return "Permission denied";
    }
    return "Unknown error";
  }
}

export type ApiResult<T> = Result<T, ApiError>;

const mediaType = (contentType: string): string => contentType.split(";", 1)[0];
const parseBody = (response: Response): Promise<any> => {
  return mediaType(response.headers.get("Content-Type") || "") ===
    "application/json"
    ? response.json()
    : response.text();
};

const parseResponse = async (response: Response): Promise<ApiResult<any>> => {
  let body = await parseBody(response);
  if (response.status >= 300) {
    console.log("Response: %d - %s", response.status, JSON.stringify(body));
  }
  if (response.status < 300) {
    return success(body);
  }
  switch (response.status) {
    case 503:
      return failure(new ApiError(FailureType.ServiceUnavailable));
    case 501:
      return failure(new ApiError(FailureType.NotImplemented));
    case 500:
      return failure(new ApiError(FailureType.ServerError));
    case 400:
      return failure(new ApiError(FailureType.BadRequest));
    case 401:
      return failure(new ApiError(FailureType.Unauthorized));
    case 402:
      return failure(new ApiError(FailureType.Forbidden));
    case 404:
      return failure(new ApiError(FailureType.NotFound));
    case 407:
      return failure(new ApiError(FailureType.ProxyAuthenticationRequired));
    case 408:
      return failure(new ApiError(FailureType.RequestTimeout));
    case 418:
      return failure(new ApiError(FailureType.ImATeapot));
  }
  return failure(new ApiError(FailureType.UnknownError));
};

export type MRFilter = {
  state?: MergeRequestState;
  order_by?: "created_at" | "updated_at";
  sort?: "asc" | "desc";
  milestone?: string;
  view?: "simple";
  labels?: string[];
  created_before?: string;
  created_after?: string;
  updated_before?: string;
  updated_after?: string;
  scope?: "created_by_me" | "assigned_to_me" | "all";
  author_id?: number;
  assignee_id?: number;
  approver_ids?: number[] | "none" | "any";
  my_reaction_emoji?: string;
  source_branch?: string;
  target_branch?: string;
  search?: string;
  in?: "title,description" | "title" | "description";
  wip?: "yes" | "no";
};

export type ProjectFilter = {
  archived?: boolean;
  visibility?: "public" | "internal" | "private";
  order_by?:
    | "id"
    | "name"
    | "path"
    | "created_at"
    | "updated_at"
    | "last_activity_at";
  sort?: "asc" | "desc";
  search?: string;
  simple?: boolean;
  owned?: boolean;
  membership?: boolean;
  starred?: boolean;
  statistics?: boolean;
  with_custom_attributes?: boolean;
  with_issues_enabled?: boolean;
  with_merge_requests_enabled?: boolean;
  with_programming_language?: string;
  wiki_checksum_failed?: boolean;
  repository_checksum_failed?: boolean;
  min_access_level?: number;
  id_after?: number;
  id_before?: number;
};

const toQueryString = (params: any) => {
  if (typeof params !== "object" || Array.isArray(params)) {
    return "";
  }

  let queryParams = [];

  for (const prop in params) {
    const value = queryParamValue(params[prop]);
    if (params !== null) {
      queryParams.push(`${sanitize(prop)}=${value}`);
    }
  }
  return queryParams.join("&");
};

const sanitize = (str: string): string => {
  return encodeURIComponent(str);
};

const queryParamValue = (value: any): string | null => {
  switch (typeof value) {
    case "object":
      if (Array.isArray(value)) {
        return value.map((x) => sanitize(x.toString())).join(",");
      }
      return null;
    case "string":
    case "bigint":
    case "boolean":
    case "number":
      return sanitize(value.toString());
  }
  return null;
};

export type Credentials = {
  instance: string;
  token: string;
};

// Base services and utilities for accessing the gitlab API.
export class Api extends EventEmitter {
  constructor(private _credentials: Credentials) {
    super();
  }

  public setCredentials(credentials: Credentials) {
    this._credentials = credentials;
  }

  private fullUri(uri: string, params?: string): string {
    let queryString = (params && "?" + toQueryString(params)) || "";
    return `${this._credentials.instance}/api/v4/${uri}${queryString}`;
  }

  get(uri: string, params?: any): Promise<ApiResult<any>> {
    const fullUri = this.fullUri(uri, params);
    console.log(fullUri);

    let args = {
      uri,
      method: "GET",
      cancel: false,
    };
    this.emit("beforecall", args);
    if (args.cancel) {
      return Promise.resolve(
        failure(new ApiError(FailureType.ExecutionBlocked))
      );
    }

    return fetch(fullUri, {
      headers: { "PRIVATE-TOKEN": this._credentials.token },
    })
      .then(parseResponse)
      .catch((err) => failure(new ApiError(FailureType.CouldNotConnect)))
      .then((x) => {
        if (!x.success) {
          this.emit("error", x.details);
        }
        return x;
      });
  }

  delete(uri: string, params?: any): Promise<ApiResult<any>> {
    const fullUri = this.fullUri(uri, params);
    console.log(fullUri);

    let args = {
      uri,
      method: "DELETE",
      cancel: false,
    };
    this.emit("beforecall", args);
    if (args.cancel) {
      return Promise.resolve(
        failure(new ApiError(FailureType.ExecutionBlocked))
      );
    }

    return fetch(fullUri, {
      method: "DELETE",
      headers: { "PRIVATE-TOKEN": this._credentials.token },
    })
      .then(parseResponse)
      .catch((err) => failure(new ApiError(FailureType.CouldNotConnect)))
      .then((x) => {
        if (!x.success) {
          this.emit("error", x.details);
        }
        return x;
      });
  }

  post(uri: string, data?: any, params?: any): Promise<ApiResult<any>> {
    const fullUri = this.fullUri(uri, params);
    let headers: { [idx: string]: string } = {
      "PRIVATE-TOKEN": this._credentials.token,
    };

    let args = {
      uri,
      method: "POST",
      cancel: false,
    };
    this.emit("beforecall", args);
    if (args.cancel) {
      return Promise.resolve(
        failure(new ApiError(FailureType.ExecutionBlocked))
      );
    }

    if (typeof data === "object") {
      headers["Content-Type"] = "application/json";
    }
    return fetch(fullUri, {
      method: "POST",
      body: data && JSON.stringify(data),
      headers,
    })
      .then(parseResponse)
      .catch((err) => failure(new ApiError(FailureType.CouldNotConnect)))
      .then((x) => {
        if (!x.success) {
          this.emit("error", x.details);
        }
        return x;
      });
  }

  mergeRequests(query?: MRFilter): Promise<ApiResult<MergeRequestSummary[]>> {
    return this.get("merge_requests", query);
  }

  projects(query?: ProjectFilter): Promise<ApiResult<Project[]>> {
    return this.get("projects", query);
  }

  groups(): Promise<ApiResult<Group[]>> {
    return this.get("groups");
  }

  self(): Promise<ApiResult<FullUser>> {
    return this.get("user");
  }
}

export class ProjectApi {
  constructor(private api: Api, private projectId: number) {}

  private uri(fragment: string): string {
    return `projects/${this.projectId}/${fragment}`;
  }

  mergeRequests(query?: MRFilter): Promise<ApiResult<MergeRequestSummary[]>> {
    return this.api.get(this.uri("merge_requests"), query);
  }

  rawFile(path: string, ref: string): Promise<ApiResult<string>> {
    return this.api.get(
      this.uri(`repository/files/${encodeURIComponent(path)}/raw`),
      { ref }
    );
  }
}

export class GroupApi {
  constructor(private api: Api, private groupId: number) {}

  private uri(fragment: string): string {
    return `groups/${this.groupId}/${fragment}`;
  }

  mergeRequests(query?: MRFilter): Promise<ApiResult<MergeRequestSummary[]>> {
    return this.api.get(this.uri("merge_requests"), query);
  }
}

export class MergeRequestApi {
  constructor(
    private api: Api,
    private project_id: number,
    private mr_iid: number
  ) {}

  private uri(uri: string) {
    return `projects/${this.project_id}/merge_requests/${this.mr_iid}/${uri}`;
  }

  changes(): Promise<ApiResult<MergeRequest & Changes>> {
    return this.api.get(this.uri("changes"));
  }

  accept(): Promise<ApiResult<void>> {
    return this.api.post(this.uri("merge"));
  }

  rebase(): Promise<ApiResult<void>> {
    return this.api.post(this.uri("rebase"));
  }

  approve(): Promise<ApiResult<void>> {
    return this.api.post(this.uri("approve"));
  }

  unapprove(): Promise<ApiResult<void>> {
    return this.api.post(this.uri("unapprove"));
  }

  // Gets all the discussions for this merge request.
  discussions(): Promise<ApiResult<Discussion[]>> {
    return this.api.get(this.uri("discussions"));
  }

  newDiscussion(
    discussion: { body: string } & DiffNote
  ): Promise<ApiResult<void>> {
    return this.api.post(this.uri("discussions"), discussion);
  }
}

export type DiscussionParent =
  | "issues"
  | "merge_requests"
  | "epics"
  | "snippets"
  | "commits";

export class DiscussionApi {
  constructor(
    private api: Api,
    private project_id: number,
    private parent: DiscussionParent,
    private parent_id: number,
    private discussion_id: number
  ) {}

  private uri(uri: string) {
    return (
      `projects/${this.project_id}/${this.parent}/${this.parent_id}/discussions/${this.discussion_id}/` +
      uri
    );
  }

  resolve(): Promise<ApiResult<void>> {
    return this.api.post(this.uri("resolve"), undefined, { resolved: true });
  }

  unresolve(): Promise<ApiResult<void>> {
    return this.api.post(this.uri("resolve"), undefined, { resolved: false });
  }

  appendNote(body: string): Promise<ApiResult<void>> {
    return this.api.post(this.uri("notes"), { body });
  }

  editNote(noteId: number, body: string): Promise<ApiResult<void>> {
    return this.api.post(this.uri(`notes/${noteId}`), { body });
  }

  deleteNote(noteId: number): Promise<ApiResult<void>> {
    return this.api.delete(this.uri(`notes/${noteId}`));
  }
}
