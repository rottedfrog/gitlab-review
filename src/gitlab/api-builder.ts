import {
  Api,
  ProjectApi,
  GroupApi,
  MergeRequestApi,
  DiscussionApi,
  DiscussionParent
} from "./api";

export class ApiBuilder {
  constructor(private _api: Api) {}

  get api(): Api {
    return this._api;
  }

  project(projectId: number): ProjectApiBuilder {
    return new ProjectApiBuilder(this._api, projectId);
  }

  group(groupId: number): GroupApiBuilder {
    return new GroupApiBuilder(this._api, groupId);
  }
}

export class ProjectApiBuilder {
  constructor(private _api: Api, private _projectId: number) {}

  get api(): ProjectApi {
    return new ProjectApi(this._api, this._projectId);
  }

  mergeRequest(mr_iid: number): MergeRequestApiBuilder {
    return new MergeRequestApiBuilder(this._api, this._projectId, mr_iid);
  }
}

export class GroupApiBuilder {
  constructor(private _api: Api, private _groupId: number) {}

  get api(): GroupApi {
    return new GroupApi(this._api, this._groupId);
  }
}

export class MergeRequestApiBuilder {
  constructor(
    private _api: Api,
    private _projectId: number,
    private _mr_iid: number
  ) {}

  get api(): MergeRequestApi {
    return new MergeRequestApi(this._api, this._projectId, this._mr_iid);
  }

  discussion(discussionId: number): DiscussionApiBuilder {
    return new DiscussionApiBuilder(
      this._api,
      this._projectId,
      "merge_requests",
      this._mr_iid,
      discussionId
    );
  }
}

export class DiscussionApiBuilder {
  constructor(
    private _api: Api,
    private _projectId: number,
    private _parent: DiscussionParent,
    private _parentId: number,
    private _discussion_id: number
  ) {}

  get api(): DiscussionApi {
    return new DiscussionApi(
      this._api,
      this._projectId,
      this._parent,
      this._parentId,
      this._discussion_id
    );
  }
}
