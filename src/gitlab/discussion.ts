import { User } from "./user";

export type DiscussionNote = {
  type: "DiscussionNote" | null;
};

export type NoteableType =
  | "Commit"
  | "Epic"
  | "Issue"
  | "MergeRequest"
  | "Snippet";

export type BaseNote = {
  id: string;
  body: string;
  attachment: string | null;
  author: User;
  created_at: string;
  updated_at: string;
  system: false;
  noteable_id: number;
  noteable_type: NoteableType;
  resolvable: boolean;
  noteable_iid: number;
};

export type CommentLinePos = {
  line_code: string;
  type: string;
};

export type LineRange = {
  start?: CommentLinePos;
  end?: CommentLinePos;
};

export type TextPos = {
  position_type: "text";
  old_line: number | null;
  new_line: number | null;
  line_range?: LineRange;
};

export type ImagePos = {
  position_type: "image";
  width: number;
  height: number;
  x: number;
  y: number;
};

export type CommentPosition = {
  base_sha: string;
  start_sha: string;
  head_sha: string;
  old_path: string;
  new_path: string;
} & (TextPos | ImagePos);

export type DiffNote = {
  type: "DiffNote";
  position: CommentPosition;
};

export type Note = BaseNote & (DiscussionNote | DiffNote);

export type Discussion = {
  id: number;
  individual_note: boolean;
  notes: Note[];
};
