export const unwrap = <T, U, V>(result: Result<T, U>, failValue: V): T | V => {
  return result.success ? result.result : failValue;
};

export function success<T>(result: T): Success<T> {
  return {
    success: true,
    result,
    map: <R>(mapFn: (data: T) => R) => {
      return success(mapFn(result));
    }
  };
}

export function failure<E>(details: E): Failure<E> {
  return {
    success: false,
    details,
    map: function() {
      return this;
    }
  };
}

export type Success<T> = {
  success: true;
  result: T;
  map: <R>(mapFn: (data: T) => R) => Success<R>;
};

export type Failure<E> = {
  success: false;
  details: E;
  map: () => Failure<E>;
};

export type Result<T, E> = Success<T> | Failure<E>;
