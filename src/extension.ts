import * as vscode from "vscode";
import { QueryViewProvider } from "./query-view-provider";
import { MergeRequestOutlineViewProvider } from "./merge-request-outline-view-provider";
import { createMergeRequestView } from "./merge-request-web-view";
import { GitLabContentProvider } from "./gitlab-content-provider";
import { Resources } from "./resources";
import { NoteComment } from "./comments";
import * as gitlab from "./gitlab";
import {
  credentials,
  updateCredentials,
  VscodeGitlabService,
} from "./vscode-gitlab-service";

const commentAuthor = (user: gitlab.User): vscode.CommentAuthorInformation => {
  return {
    name: user.name,
    iconPath:
      (user.avatar_url && vscode.Uri.parse(user.avatar_url)) || undefined,
  };
};

const showResultMessage = (
  result: gitlab.ApiResult<void>,
  successMessage: string,
  failMessage: string,
  name: string | undefined
) => {
  const suffix = name ? ": " + name : "";
  if (result.success) {
    vscode.window.showInformationMessage(`${successMessage}${suffix}`);
  } else {
    vscode.window.showErrorMessage(
      `${failMessage} (${result.details})${suffix}`
    );
  }
};

const promptCredentials = async () => {
  let creds = credentials();
  if (!creds.token) {
    creds.instance =
      (await vscode.window.showInputBox({
        prompt: "Gitlab server",
        placeHolder: "https::/gitlab.com",
        value: creds.instance,
      })) || "";
    creds.token =
      (await vscode.window.showInputBox({
        prompt: "Gitlab API Token",
        value: creds.token,
      })) || "";
    updateCredentials(creds);
  }
};

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export async function activate(context: vscode.ExtensionContext) {
  promptCredentials();
  const gitlab = new VscodeGitlabService(context);
  await gitlab.init();
  const api = gitlab.api();

  api.addListener("error", (err: gitlab.ApiError) =>
    vscode.window.showErrorMessage(`Gitlab Merge Requests: ${err.toString()}`)
  );
  const resources = new Resources(context);
  const qvp = new QueryViewProvider(gitlab, resources);

  context.subscriptions.push(
    vscode.window.registerTreeDataProvider("merge-request-queries", qvp)
  );

  context.subscriptions.push(
    vscode.commands.registerCommand(
      "gitlabReview.showMergeRequestDetails",
      (mergeRequest) => createMergeRequestView(api, mergeRequest)
    )
  );

  context.subscriptions.push(
    vscode.workspace.registerTextDocumentContentProvider(
      GitLabContentProvider.scheme,
      new GitLabContentProvider(api)
    )
  );

  let outlineProvider = new MergeRequestOutlineViewProvider(gitlab, resources);
  vscode.commands.registerCommand(
    "gitlabReview.approveMergeRequest",
    async () =>
      outlineProvider.api &&
      showResultMessage(
        await outlineProvider.api.approve(),
        "Approved",
        "Approval Failed",
        outlineProvider.name()
      )
  );

  vscode.commands.registerCommand(
    "gitlabReview.unapproveMergeRequest",
    async () =>
      outlineProvider.api &&
      showResultMessage(
        await outlineProvider.api.unapprove(),
        "Approval Revoked",
        "Revoke Approval Failed",
        outlineProvider.name()
      )
  );
  vscode.commands.registerCommand(
    "gitlabReview.acceptMergeRequest",
    async () =>
      outlineProvider.api &&
      showResultMessage(
        await outlineProvider.api.accept(),
        "MR Accepted",
        "Unable to Accept MR",
        outlineProvider.name()
      )
  );

  vscode.commands.registerCommand(
    "gitlabReview.rebaseMergeRequest",
    async () =>
      outlineProvider.api &&
      showResultMessage(
        await outlineProvider.api.rebase(),
        "Rebased",
        "Unable to Rebase Automatically",
        outlineProvider.name()
      )
  );

  vscode.commands.registerCommand(
    "gitlabReview.setCurrentMergeRequest",
    (mergeRequest) => outlineProvider.init(mergeRequest)
  );

  vscode.commands.registerCommand("gitlabReview.refreshQueries", () => {
    qvp.refresh();
  });

  vscode.commands.registerCommand(
    "gitlabReview.saveComment",
    (note: NoteComment) => {
      if (note.thread) {
        outlineProvider.comments?.saveNote(
          note.thread,
          note.gitlabNote.noteable_iid,
          note.body.toString()
        );
      }
    }
  );

  vscode.commands.registerCommand(
    "gitlabReview.cancelComment",
    (note: NoteComment) => {
      if (note.thread) {
        note.thread.comments = note.thread.comments.map((n) => {
          if (
            (n as NoteComment).gitlabNote.noteable_iid ===
            note.gitlabNote.noteable_iid
          ) {
            n.mode = vscode.CommentMode.Preview;
          }
          return n;
        });
      }
    }
  );

  vscode.commands.registerCommand(
    "gitlabReview.openMergeRequestInBrowser",
    (mr: gitlab.MergeRequestSummary | undefined) => {
      if (outlineProvider.mergeRequest?.web_url) {
        vscode.commands.executeCommand(
          "vscode.open",
          vscode.Uri.parse(outlineProvider.mergeRequest.web_url, true)
        );
      }
    }
  );

  vscode.commands.registerCommand(
    "gitlabReview.deleteComment",
    (note: NoteComment) => {
      if (note.thread) {
        outlineProvider.comments?.deleteNote(
          note.thread,
          note.gitlabNote.noteable_iid
        );
      }
    }
  );

  vscode.commands.registerCommand(
    "gitlabReview.editComment",
    (note: NoteComment) => {
      if (note.thread) {
        note.thread.comments = note.thread.comments.map((n) => {
          if (
            (n as NoteComment).gitlabNote.noteable_iid ===
            note.gitlabNote.noteable_iid
          ) {
            n.mode = vscode.CommentMode.Editing;
          }
          return n;
        });
      }
    }
  );

  vscode.commands.registerCommand(
    "gitlabReview.newDiscussion",
    (reply: vscode.CommentReply) => {
      const user = gitlab.user();
      if (user) {
        outlineProvider.comments?.createThread(
          reply.thread.uri,
          reply.thread.range.start.line,
          reply.text,
          commentAuthor(user)
        );
      }
    }
  );

  vscode.commands.registerCommand(
    "gitlabReview.replyDiscussion",
    (reply: vscode.CommentReply) => {
      const user = gitlab.user();
      if (user) {
        outlineProvider.comments?.reply(reply, commentAuthor(user));
      }
    }
  );

  vscode.commands.registerCommand(
    "gitlabReview.resolveDiscussion",
    async (reply: vscode.CommentReply) => {
      const user = gitlab.user();
      if (user) {
        const replyResult = await outlineProvider.comments?.reply(
          reply,
          commentAuthor(user)
        );
        if (replyResult && replyResult.success) {
          outlineProvider.comments?.resolve(reply, commentAuthor(user));
        }
      }
    }
  );

  context.subscriptions.push(
    vscode.window.registerTreeDataProvider(
      "merge-request-details",
      outlineProvider
    )
  );
}

// this method is called when your extension is deactivated
export function deactivate() {}
