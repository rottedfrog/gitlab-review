import * as vscode from "vscode";
import * as gitlab from "./gitlab";
import { gitlabEncode } from "./gitlab-content-provider";
import * as parseDiff from "parse-diff";

const headToUri = (
  mr: gitlab.MergeRequest,
  change: gitlab.Change
): vscode.Uri => {
  if (!mr.diff_refs.head_sha) {
    throw new Error("Unexpected missing head sha");
  }
  return gitlabEncode(mr.project_id, change.new_path, mr.diff_refs.head_sha);
};

const author = (user: gitlab.User): vscode.CommentAuthorInformation => {
  return {
    name: user.name,
    iconPath:
      (user.avatar_url && vscode.Uri.parse(user.avatar_url)) || undefined,
  };
};

const new_line_to_old = (
  change: gitlab.Change,
  new_line: number
): number | undefined => {
  if (change.new_file) {
    return undefined;
  }

  if (change.deleted_file || !change.diff) {
    return new_line;
  }

  return translate_new_line_to_old(change.diff, new_line);
};

const translate_from_hunk = (
  hunk: parseDiff.Chunk,
  new_line: number
): number | undefined => {
  let last_deleted = undefined;
  for (const line of hunk.changes) {
    if (line.type === "add") {
      if (line.ln == new_line) {
        return last_deleted;
      }
      last_deleted = undefined;
    } else if (line.type === "del") {
      last_deleted = line.ln;
    } else {
      last_deleted = undefined;
      if (line.ln2 == new_line) {
        return line.ln1;
      }
    }
  }
  return undefined;
};

const translate_new_line_to_old = (
  diffStr: string,
  new_line: number
): number | undefined => {
  try {
    diffStr = "+++ file.txt\n--- file.txt\n" + diffStr;
    console.log(diffStr);
    const diff = parseDiff(diffStr.replace("\\n", "\n"));
    let change: number = 0;
    for (const hunk of diff[0].chunks) {
      if (hunk.newStart > new_line) {
        break;
      }
      if (hunk.newStart + hunk.newLines >= new_line) {
        let rows = translate_from_hunk(hunk, new_line);
        if (rows === undefined) {
          return undefined;
        }
        change += rows;
        break;
      }
      change += hunk.oldLines - hunk.newLines;
    }
    return new_line + change;
  } catch (err) {
    console.log(err);
  }
};

export type NoteComment = {
  label?: string;
  gitlabNote: gitlab.Note;
  body: string | vscode.MarkdownString;
  mode: vscode.CommentMode;
  author: vscode.CommentAuthorInformation;
  thread?: vscode.CommentThread;
  contextValue?: string;
};

const toComment = (
  note: gitlab.Note,
  currentUser: gitlab.User
): NoteComment => {
  return {
    body: new vscode.MarkdownString(note.body),
    mode: vscode.CommentMode.Preview,
    author: author(note.author),
    contextValue: note.author.id === currentUser.id ? "editable" : "",
    gitlabNote: note,
  };
};

const position = (
  discussion: gitlab.Discussion
): gitlab.CommentPosition | undefined =>
  (discussion.notes.filter((x) => x.type === "DiffNote")[0] as gitlab.DiffNote)
    ?.position;

const threadId = (thread: vscode.CommentThread): string => {
  return `${thread.uri.toString()}||${thread.range.start.line}`;
};

export class MRCommentManager implements vscode.CommentingRangeProvider {
  private commentController: vscode.CommentController;
  private threadToDiscussion: { [uriRange: string]: gitlab.Discussion };
  private changes: { [uri: string]: gitlab.Change | undefined };
  constructor(
    private mr: gitlab.MergeRequest & gitlab.Changes,
    private apiBuilder: gitlab.MergeRequestApiBuilder,
    private discussions: gitlab.Discussion[],
    currentUser: gitlab.User
  ) {
    this.commentController = vscode.comments.createCommentController(
      `gitlab-discussions-${mr.id}`,
      `Gitlab Discussions (MR !${mr.iid})`
    );
    this.threadToDiscussion = {};
    this.changes = {};

    discussions.forEach((discussion) => {
      const pos = position(discussion);
      if (pos?.position_type === "text") {
        const uri = gitlabEncode(mr.project_id, pos.new_path, pos.head_sha);
        const new_line = pos.new_line || 1;
        const range = new vscode.Range(new_line - 1, 0, new_line - 1, 0);
        const notes = discussion.notes.map((x) => toComment(x, currentUser));
        const thread = this.commentController.createCommentThread(
          uri,
          range,
          notes
        );
        notes.forEach((n) => (n.thread = thread));
        thread.collapsibleState = vscode.CommentThreadCollapsibleState.Expanded;
        if (discussion.notes.find((x) => x.resolvable)) {
          thread.contextValue = "resolveable";
        } else {
          thread.contextValue = "resolved";
          thread.label = "Resolved";
        }

        this.threadToDiscussion[threadId(thread)] = discussion;
      }
    });

    this.changes = mr.changes.reduce(
      (result: { [idx: string]: gitlab.Change }, change) => {
        const uri = headToUri(mr, change).toString();
        result[uri] = change;
        return result;
      },
      {}
    );

    this.commentController.commentingRangeProvider = this;
  }

  provideCommentingRanges(
    document: vscode.TextDocument,
    token: vscode.CancellationToken
  ): vscode.ProviderResult<vscode.Range[]> {
    if (this.changes[document.uri.toString()]) {
      return [
        new vscode.Range(
          0,
          0,
          document.lineCount - 1,
          document.lineAt(document.lineCount - 1).text.length
        ),
      ];
    }
    return null;
  }

  async createThread(
    uri: vscode.Uri,
    line: number,
    body: string,
    author: vscode.CommentAuthorInformation
  ): Promise<gitlab.ApiResult<vscode.CommentThread>> {
    const change = this.changes[uri.toString()];
    if (change && this.mr.diff_refs.base_sha) {
      let discussion = {
        body,
        type: "DiffNote" as const,
        position: {
          ...this.mr.diff_refs,
          position_type: "text" as const,
          new_line: line + 1,
          new_path: change.new_path,
          old_path: change.old_path,
          old_line: new_line_to_old(change, line + 1) || null,
        },
      };
      const result = await this.apiBuilder.api.newDiscussion(discussion);
      if (!result.success) {
        return result;
      }
      let thread = this.commentController.createCommentThread(
        uri,
        new vscode.Range(line, 0, line, 0),
        [
          {
            body,
            mode: vscode.CommentMode.Preview,
            author,
            contextValue: "editable",
          },
        ]
      );
      return gitlab.success(thread);
    } else {
      throw new Error("Shit! I can't find the change");
    }
  }

  async reply(
    reply: vscode.CommentReply,
    author: vscode.CommentAuthorInformation
  ): Promise<gitlab.ApiResult<vscode.CommentThread>> {
    const result = await this.api(reply.thread).appendNote(reply.text);
    if (result.success) {
      reply.thread.comments = [
        ...reply.thread.comments,
        {
          body: reply.text,
          author,
          mode: vscode.CommentMode.Preview,
          contextValue: "editable",
        },
      ];
    }
    return gitlab.success(reply.thread);
  }

  private api(thread: vscode.CommentThread): gitlab.DiscussionApi {
    const discussion = this.threadToDiscussion[threadId(thread)];
    return this.apiBuilder.discussion(discussion.id).api;
  }

  async saveNote(
    thread: vscode.CommentThread,
    iid: number,
    body: string
  ): Promise<gitlab.ApiResult<void>> {
    const discussion = this.threadToDiscussion[threadId(thread)];
    const result = await this.apiBuilder
      .discussion(discussion.id)
      .api.editNote(iid, body);
    if (result.success) {
      thread.comments = thread.comments.map((n) => {
        if ((n as NoteComment).gitlabNote.noteable_iid === iid) {
          n.body = body;
        }
        return n;
      });
    }
    return result;
  }

  async deleteNote(
    thread: vscode.CommentThread,
    iid: number
  ): Promise<gitlab.ApiResult<void>> {
    const discussion = this.threadToDiscussion[threadId(thread)];
    const result = await this.apiBuilder
      .discussion(discussion.id)
      .api.deleteNote(iid);
    if (result.success) {
      thread.comments = thread.comments.filter(
        (n) => (n as NoteComment).gitlabNote.noteable_iid === iid
      );
    }
    return result;
  }

  async unresolve(
    reply: vscode.CommentReply,
    author: vscode.CommentAuthorInformation
  ): Promise<gitlab.ApiResult<vscode.CommentReply>> {
    if (reply.text) {
      const result = await this.reply(reply, author);
    }
    const result = await this.api(reply.thread).unresolve();
    if (result.success) {
      reply.thread.contextValue =
        (reply.thread.contextValue || "") + " resolveable";
      reply.thread.label = "";
      return gitlab.success(reply);
    }
    return result;
  }

  async resolve(
    reply: vscode.CommentReply,
    author: vscode.CommentAuthorInformation
  ): Promise<gitlab.ApiResult<vscode.CommentReply>> {
    if (reply.text) {
      const result = await this.reply(reply, author);
    }
    const result = await this.api(reply.thread).resolve();
    if (result.success) {
      reply.thread.contextValue =
        (reply.thread.contextValue || "") + " resolved";
      reply.thread.label = "Resolved";
      return gitlab.success(reply);
    }
    return result;
  }

  dispose() {
    this.commentController.dispose();
  }
}
