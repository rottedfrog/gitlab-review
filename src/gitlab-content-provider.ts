import * as vscode from "vscode";
import * as gitlab from "./gitlab";

export const gitlabEncode = (
  projectId: number,
  path: string,
  ref: string
): vscode.Uri => vscode.Uri.parse(`gitlab://${projectId}/${path}#${ref}`, true);

export class GitLabContentProvider
  implements vscode.TextDocumentContentProvider {
  constructor(private api: gitlab.Api) {}

  static scheme = "gitlab";

  provideTextDocumentContent(
    uri: vscode.Uri,
    cancellationToken: vscode.CancellationToken
  ): vscode.ProviderResult<string> {
    const apiBuilder = new gitlab.ApiBuilder(this.api);
    if (uri.scheme === GitLabContentProvider.scheme) {
      const projectId = Number.parseInt(uri.authority);
      const path = uri.path.startsWith("/") ? uri.path.substr(1) : uri.path;
      const ref = uri.fragment;
      return apiBuilder
        .project(projectId)
        .api.rawFile(path, ref)
        .then((res) => {
          return res.success ? res.result : "";
        });
    }
    return null;
  }
}
