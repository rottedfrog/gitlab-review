import * as vscode from "vscode";
import * as gitlab from "./gitlab";

const MergeRequestTemplate = `
<html>
  <head>
    <style>
        html { font-family: "Segoe UI","Helvetica Neue","Helvetica",Arial,sans-serif }
        h1, h2, h3 { font-weight: 100; }
    </style>
  </head>
  <body>
    <h1 class="caption">{{title}}</h1>
    <caption>
      Created by <img href="{{#author.avatar_url}}"> {{author.name}} at {{created_at}}
    </caption>
    {!<caption>
      Merged by {{merged_by.name}} at {{merged_at}} 
     </caption>!}
     {!<caption>
      Closed by {{closed_by.name}} at {{closed_at}} 
     </caption>!}
    <div>{{upvotes}}</div><div>{{downvotes}}</div>
    <summary>{{description}}</summary>
  </body>
</html>
`;

export const createMergeRequestView = (
  api: gitlab.Api,
  mergeRequest: gitlab.MergeRequest
): vscode.WebviewPanel => {
  const panel = vscode.window.createWebviewPanel(
    "gitlabMergeRequest",
    `Merge Request #${mergeRequest.iid}`,
    vscode.ViewColumn.One
  );

  panel.webview.html = createHtmlFromTemplate(
    mergeRequest,
    "",
    MergeRequestTemplate
  );
  return panel;
};

const escapeHtml = (text: string): string => {
  var map: { [key: string]: string } = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': "&quot;",
    "'": "&#039;",
  };

  return text.replace(/[&<>"']/g, (m) => map[m]);
};

const validUri = (maybe_uri: string): string => {
  try {
    return vscode.Uri.parse(maybe_uri, true).toString();
  } catch (err) {
    return "";
  }
};

const createHtmlFromTemplate = (
  props: any,
  prefix = "",
  template: string
): string => {
  let html = template;
  for (let prop in props) {
    let value = props[prop];
    if (typeof value === "object") {
      html = createHtmlFromTemplate(value, prefix + prop + ".", html);
    } else if (value) {
      html = html.replace(`{{${prefix}${prop}}}`, escapeHtml(value.toString()));
      html = html.replace(`{{#${prefix}${prop}}}`, () => validUri(value));
    }
  }

  // Remove conditional sections containing unsubstituted template expressions
  html = html.replace(/\{!.*?!\}/gs, (conditional) => {
    console.log(conditional);
    return conditional.indexOf("{{") >= 0
      ? ""
      : conditional.substring(2, conditional.length - 2);
  });

  return html;
};
