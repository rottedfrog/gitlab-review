import * as vscode from "vscode";

export class Resources {
  constructor(private context: vscode.ExtensionContext) {}

  get error(): vscode.Uri {
    return vscode.Uri.file(this.context.asAbsolutePath("media/error.svg"));
  }

  get statusAdded(): vscode.Uri {
    return vscode.Uri.file(
      this.context.asAbsolutePath("media/status-added.svg")
    );
  }

  get statusDeleted(): vscode.Uri {
    return vscode.Uri.file(
      this.context.asAbsolutePath("media/status-deleted.svg")
    );
  }

  get statusModified(): vscode.Uri {
    return vscode.Uri.file(
      this.context.asAbsolutePath("media/status-modified.svg")
    );
  }

  get statusRenamed(): vscode.Uri {
    return vscode.Uri.file(
      this.context.asAbsolutePath("media/status-renamed.svg")
    );
  }
}
