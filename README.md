# gitlab-review

Review GitLab merge requests directly from VSCode. - this doesn't just open them in the browser, it will open the diffs for every file directly inside VSCode, show you all the comments allow you to create and reply to discussions, resolve comments and merge them, without leaving VS Code.

There is a lot still to do, and it's still being developed, but it's starting to get useful. Please feel free to log issues, suggest features (on GitLab, naturally).

## Features

- Find merge requests by project, group, or those assigned to you
- Review all the changes in VS Code with highlighted differences
- Comment on any line, reply to or resolve discussion threads
- Approve, rebase and merge from inside VS Code
- Use it with gitlab.com or any hosted instance of GitLab.

\!\[feature X\]\(images/feature-x.png\)

## Requirements

You'll need a GitLab personal access token with `api` access to use this extension. You can create one by following the
instructions [here](http://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)

## Extension Settings

This extension contributes the following settings:

- `gitlabReview.instance`: Which instance of GitLab to use. by default, uses https://gitlab.com.
- `gitlabReview.token`: The personal access token to use.

## Known Issues

- internal links in comments don't work yet.

## Release Notes

### 0.1

Initial preview release of GitLab Review
